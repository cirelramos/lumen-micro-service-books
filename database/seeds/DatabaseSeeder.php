<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {

        // $this->call('UsersTableSeeder');
        factory( \App\Http\Book::class, 50 )->create();
    }
}
