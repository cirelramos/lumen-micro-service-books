<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAuthorsTable
 */
class CreateBooksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {

        Schema::create( 'books', function ( Blueprint $table ) {

            $table->increments( 'id_book' );
            $table->string( 'title' );
            $table->string( 'description' );
            $table->string( 'price' );
            $table->integer( 'id_author' )->unsigned();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {

        Schema::dropIfExists( 'books' );
    }
}
