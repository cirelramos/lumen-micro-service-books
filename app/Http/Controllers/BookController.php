<?php

namespace App\Http\Controllers;

use App\Http\Book;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * Class bookController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{

    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware( 'api.auth' );
    }

    public function index()
    {

        $books = Book::all();

        return $this->successResponse( $books, '', Response::HTTP_OK );

    }

    public function store( Request $request )
    {

        $rules = [
            'title'       => 'required|max:255',
            'description' => 'required|max:255',
            'price'       => 'required|min:1',
            'id_author'   => 'required|min:1',
        ];

        $this->validate( $request, $rules );

        $book = Book::create( $request->all() );

        return $this->successResponse( $book, 'creado exitosamente', Response::HTTP_CREATED );

    }

    public function show( Request $request, $id )
    {

        $book = Book::findOrFail( $id );

        return $this->successResponse( $book, 'se encontro el book', Response::HTTP_OK );

    }

    /**
     * @param Request $request
     * @param         $idbook
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update( Request $request, $idbook ): JsonResponse
    {

        $rules = [
            'title'       => 'required|max:255',
            'description' => 'required|max:255',
            'price'       => 'required|min:1',
            'id_author'   => 'required|min:1',
        ];

        $book = Book::findOrFail( $idbook );

        $this->validate( $request, $rules );

        $book->fill( $request->all() );

        if ( $book->isClean() ) {
            return $this->errorResponse( [], 'debe cambiar algun valor', Response::HTTP_UNPROCESSABLE_ENTITY );
        }
        $book->save();

        return $this->successResponse( $book, 'actualizado exitosamente', Response::HTTP_ACCEPTED );
    }

    public function destroy( Request $request, $idbook )
    {

        $book = Book::findOrFail( $idbook );
        $book->delete();

        return $this->successResponse( $book, 'eliminado exitosamente', Response::HTTP_ACCEPTED );

    }

}
