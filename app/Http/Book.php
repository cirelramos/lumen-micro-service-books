<?php

namespace App\Http;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 * @package App
 */
class Book extends Model
{

    protected $primaryKey = 'id_book';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'id_author',
    ];
}
